## ModTest

Главная программа теста Go-модулей в многоуровневом публичном репо.
Тестируем предложение из https://www.linux.org.ru/forum/web-development/14941880

"
В общем, небольшая инструкция для тех, кто захотел либы для гошных проектов (при использовании модулей) размещать не по стандартному пути вида domain.com/group/project, а с разбиением на подгруппы: domain.com/group/subgroup/subsubgroup/project.

Сразу скажу, что это все выглядит так себе, но работает. Есть два варианта: когда ваша либа публичная и когда приватная. Сначала выполняете команду

git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"

, чтобы go get не просило логин и пароль от гитлаба.

Когда либа публичная. Тут все просто. Импортите либу вот так:
import "gitlab.com/group/subgroup/project"

Все, как обычно. Если Goland не подтянул ее автоматически, то вызываете

go get -v gitlab.com/group/subgroup/project

и смотрите, что пишет. Если пишет «fatal: could not read Username for ‘https://gitlab.com’: terminal prompts disabled» то выполняете «export GIT_TERMINAL_PROMPT=1» — и повторяете запуск го гет, вводя логин и пароль по запросу

Когда либа приватная. В go.mod в первой строке этой либы (не проекта, в который ее импортируете, а именно в go.mod либы) в конце дописываете .git: module gitlab.com/dimuska139testgroup/dimuska139testsubgroup/dimuska139testsubsubgroup/sendpulse-sdk.git
Затем надо добавить новый тег для этой либы. Это важно. В проекте, где либу импортите, делайте импорт вот так (тоже с .git в конце):

import "gitlab.com/dimuska139testgroup/dimuska139testsubgroup/dimuska139testsubsubgroup/sendpulse-sdk.git"

Если все равно нихрена не получилось, то удаляете у проекта go.mod и go.sum — и повторяете действия, которые я написал выше. Все другие, перепробованные мной методы, найденные в интернетах, не работают.
"